# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Jonathan Albrieux <jonathan.albrieux@gmail.com>
# Co-Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=device-bq-paella
pkgdesc="BQ Aquaris X5"
pkgver=3
pkgrel=2
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo"
subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem:kernel_mainline_modem
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem:nonfree_firmware_modem
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem() {
	pkgdesc="Close to mainline kernel (non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/WiFi/BT/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-bq-picmt-venus
		 firmware-qcom-msm8916-wcnss firmware-bq-picmt-wcnss-nv"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem() {
	pkgdesc="Modem firmware"
	depends="firmware-bq-picmt-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem"
	mkdir "$subpkgdir"
}

sha512sums="7e693384eb5ba46f14cbf57a9d9e9cffa2cd82af96f0978123fd618ca955ac3ca22ad1b3d4d2aea43ab8ee42b03dbeaa00ce0e08b5e831de9b6a0b7787e416f4  deviceinfo"
